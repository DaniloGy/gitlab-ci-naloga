//============================================================================
// Name        : K-means.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <random>
using namespace std;

class Tocka {
private:
	double x, y;
public:
	vector<double> razdalje;
	Tocka();
	Tocka(double x, double y):x(x), y(y){};

	void setX(double x){
		this->x=x;
	};

	void setY(double y){
		this->y=y;
	};

	void pushBack(double raz){
		razdalje.push_back(raz);
	};

	double getX(){
		return x;
	};

	double getY(){
		return y;
	};

	double razdalja(Tocka p, double kx, double ky){
		double d;
		d=pow((kx-p.getX()),2)+pow((ky-p.getY()),2);
		return d;
	};

	double minimum(){
		double d = razdalje[0];
		for(int i = 1; i<razdalje.size(); i++){
			if(d > razdalje[i]){
				d=razdalje[i];
			}
		}
		return d;
	};

	bool isEqual(Tocka tocka){
		if(x==tocka.x && y==tocka.y)return true;
		else return false;
	};

	string toString(){
		stringstream tx, ty;
		tx<<x;
		ty<<y;

		return "(" + tx.str() + ", " + ty.str() + ")";
	};
};

class TockaK {
private:
	double x, y;
	double raz;

public:
	vector<Tocka> vec;
	TockaK();
	TockaK(double x, double y):x(x), y(y){};

	void setX(double x){
		this->x=x;
	};

	void setY(double y){
		this->y=y;
	};

	void setRaz(double raz){
		this->raz=raz;
	};

	void pushBackT(Tocka t){
		vec.push_back(t);
	};

	double getX(){
		return x;
	};

	double getY(){
		return y;
	};

	double getRaz(){
		return raz;
	};

	string toString(){
		stringstream tx, ty;
		tx<<x;
		ty<<y;

		return "(" + tx.str() + ", " + ty.str() + ")";
	};
};

bool Branje_Stevil(vector<Tocka> &vec, const char s[]) {
	ifstream input(s);
	double x, y;
	//char st;

	if (!input.is_open()) {
		return false;
	}

	while (!input.eof()) {
		input >> x;
		input >> y;
		vec.push_back(Tocka(x, y));

		/*input >> st;
		vec.push_back(st);
		while (isspace(input.peek())) input.get();*/
	}
	input.close();
	return true;
}

void Izpis_Stevi(vector<int> tocke, vector<int> gruce) {
	ofstream output("out.txt");

	for (int i = 0; i<tocke.size(); i++){
		output << tocke[i]<<" "<<gruce[i]<<endl;
	}
}

void grucenje(vector<Tocka> &P, vector<TockaK> &K, int t){
	double min;
	for(int ite=0; ite<t; ite++){
		for(int i=0; i<P.size(); i++){
			P[i].razdalje.clear();
		}
		for(int i=0; i<K.size(); i++){
			K[i].vec.clear();
		}

		for(int i = 0; i<P.size(); i++){
			for(int j = 0; j<K.size(); j++){
				K[j].setRaz(P[i].razdalja(P[i], K[j].getX(), K[j].getY()));
				P[i].pushBack(P[i].razdalja(P[i], K[j].getX(), K[j].getY()));
			}

			min = P[i].minimum();

			for(int n = 0; n<K.size(); n++){
				if(min == K[n].getRaz()){
					K[n].pushBackT(P[i]);
					n = K.size();
				}
			}
		}


		double xPoints=0;
		double yPoints=0;
		for(int i=0; i<K.size(); i++){
			for(int j=0; j<K[i].vec.size(); j++){
				xPoints += K[i].vec[j].getX();
				yPoints += K[i].vec[j].getY();
			}
			K[i].setX(xPoints/K[i].vec.size());
			K[i].setY(yPoints/K[i].vec.size());

			xPoints=0;
			yPoints=0;
		}
	}
}

void kRandom(vector<TockaK> &K, int k, vector<Tocka> P){
	double maxX=P[0].getX(), minX=P[0].getX();
	double maxY=P[0].getY(), minY=P[0].getY();
	for(int i=1; i<P.size(); i++){
		if(minX > P[i].getX()){
			minX=P[i].getX();
		}
		if(maxX < P[i].getX()){
			maxX=P[i].getX();
		}
		if(minY > P[i].getY()){
			minY=P[i].getY();
		}
		if(maxY < P[i].getY()){
			maxY=P[i].getY();
		}
	}


	std::random_device dev;
	std::mt19937 rng(dev());

	for(int j=0; j<k; j++){


		std::uniform_real_distribution<> pointX(minX, maxX);
		std::uniform_real_distribution<> pointY(minY, maxY);


		double xx = pointX(rng);
		double roundedX = std::round(10. * xx) / 10.;

		double yy = pointY(rng);
		double roundedY = std::round(10. * yy) / 10.;

		K.push_back(TockaK(roundedX, roundedY));
	}
}

void izpis(vector<Tocka> P, vector<TockaK> K, vector<int> &tocke, vector<int> &gruce){
	for(int i=0; i<P.size(); i++){
		for(int j=0; j<K.size(); j++){
			for(int n=0; n<K[j].vec.size(); n++){
				if(P[i].isEqual(K[j].vec[n])){
					gruce.push_back(j);
					tocke.push_back(i);
				}
			}
		}
	}
}


int main(int argc, const char* argv[]) {
	vector<Tocka> P;
	vector<TockaK> K;
	vector<int> tocke;
	vector<int> gruce;
	int k;
	int t;

	if (argc < 4) return 0;
	if (!Branje_Stevil(P, argv[3])) return 0;


	stringstream strT, strK;
	strT << argv[1][0];
	strT >> t;

	strK << argv[2][0];
	strK >> k;


	kRandom(K, k, P);
	grucenje(P, K, t);
	izpis(P, K, tocke, gruce);
	Izpis_Stevi(tocke, gruce);


	return 0;
}
