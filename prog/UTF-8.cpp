//============================================================================
// Name        : UTF-8.cpp
// Author      : Danilo
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <sstream>
#include <cstring>
using namespace std;

void hexvbin(vector<char> &hexvhod, vector<int> &bin){
	const char* compare;

	for(unsigned int i=0; i<hexvhod.size(); i++){
		string sym(1, hexvhod[i]);
		compare = sym.c_str();
		if(strcmp(compare, "0")==0){
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
		}else if(strcmp(compare, "1")==0){
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
		}else if(strcmp(compare, "2")==0){
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
		}else if(strcmp(compare, "3")==0){
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
		}else if(strcmp(compare, "4")==0){
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
		}else if(strcmp(compare, "5")==0){
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
		}else if(strcmp(compare, "6")==0){
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
		}else if(strcmp(compare, "7")==0){
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
		}else if(strcmp(compare, "8")==0){
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
		}else if(strcmp(compare, "9")==0){
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
		}else if(strcmp(compare, "a")==0 || strcmp(compare, "A")==0){
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
		}else if(strcmp(compare, "b")==0 || strcmp(compare, "B")==0){
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
		}else if(strcmp(compare, "c")==0 || strcmp(compare, "C")==0){
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
		}else if(strcmp(compare, "d")==0 || strcmp(compare, "D")==0){
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
		}else if(strcmp(compare, "e")==0 || strcmp(compare, "E")==0){
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
		}else if(strcmp(compare, "f")==0 || strcmp(compare, "F")==0){
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
		}else {
			cout<<"Nepravilen vnos."<<endl;
		}
	}
}



void kodiranje(vector<int> &bin){


	while(bin[0] == 0){
		bin.erase(bin.begin());
	}

	if(bin.size() < 8){
		while(bin.size() < 8)bin.insert(bin.begin(), 0);
	}
	else if(bin.size() <= 11){
		while(bin.size() < 11)bin.insert(bin.begin(), 0);
		bin.insert(bin.begin(), 0);
		bin.insert(bin.begin(), 1);
		bin.insert(bin.begin(), 1);

		bin.insert(bin.begin()+8, 0);
		bin.insert(bin.begin()+8, 1);
	}
	else if(bin.size() <= 16){
		while(bin.size() < 16)bin.insert(bin.begin(), 0);
		bin.insert(bin.begin(), 0);
		bin.insert(bin.begin(), 1);
		bin.insert(bin.begin(), 1);
		bin.insert(bin.begin(), 1);

		bin.insert(bin.begin()+8, 0);
		bin.insert(bin.begin()+8, 1);

		bin.insert(bin.begin()+16, 0);
		bin.insert(bin.begin()+16, 1);
	}
	else if(bin.size() <= 24){
		while(bin.size() < 24)bin.insert(bin.begin(), 0);
		bin.insert(bin.begin(), 0);
		bin.insert(bin.begin(), 1);
		bin.insert(bin.begin(), 1);
		bin.insert(bin.begin(), 1);
		bin.insert(bin.begin(), 1);

		bin.insert(bin.begin()+8, 0);
		bin.insert(bin.begin()+8, 1);

		bin.insert(bin.begin()+16, 0);
		bin.insert(bin.begin()+16, 1);

		bin.insert(bin.begin()+24, 0);
		bin.insert(bin.begin()+24, 1);
	}

	for(unsigned int i=0; i<bin.size(); i++){
			cout<<bin[i];
		}
	cout<<endl;

	string st="";
	vector<string> stevila;

	//cout<<"Stevila: ";
	for(unsigned int j=0; j<bin.size(); j++){
		for(int i=0; i<4; i++){
			st += to_string(bin[j+i]);
		}
		stevila.push_back(st);
		st="";
		j += 3;
	}


	cout<<"Rezultat kodiranja UTF-8 je: ";
	for(unsigned int i=0; i<stevila.size(); i++){

		if(stevila[i]=="0000"){
			cout<<0;
		}
		else if(stevila[i]=="0001"){
			cout<<1;
		}
		else if(stevila[i]=="0010"){
			cout<<2;
		}
		else if(stevila[i]=="0011"){
			cout<<3;
		}
		else if(stevila[i]=="0100"){
			cout<<4;
		}
		else if(stevila[i]=="0101"){
			cout<<5;
		}
		else if(stevila[i]=="0110"){
			cout<<6;
		}
		else if(stevila[i]=="0111"){
			cout<<7;
		}
		else if(stevila[i]=="1000"){
			cout<<8;
		}
		else if(stevila[i]=="1001"){
			cout<<9;
		}
		else if(stevila[i]=="1010"){
			cout<<"A";
		}
		else if(stevila[i]=="1011"){
			cout<<"B";
		}
		else if(stevila[i]=="1100"){
			cout<<"C";
		}
		else if(stevila[i]=="1101"){
			cout<<"D";
		}
		else if(stevila[i]=="1110"){
			cout<<"E";
		}
		else if(stevila[i]=="1111"){
			cout<<"F";
		}
	}
}



void dekodiranje(vector<int> bin){
	string st = "";
	vector<string> stevila;

	if(bin[0]==0){
		if(bin.size()<8){
			cout<<"Napaka!"<<endl;
		}
		for(unsigned int j=0; j<bin.size(); j++){
			for(int i=0; i<4; i++){
				st += to_string(bin[j+i]);
			}
			stevila.push_back(st);
			st="";
			j += 3;
		}
	}
	else if(bin[0]==1 && bin[1]==1 && bin[2]==0){
		if(bin.size()<16 || bin[8]!= 1 || bin[9]!=0){
			cout<<"Napaka!"<<endl;
		}else{
			for(int i=3; i<9; i++){
				if(bin[i]==1){
					for(int j=10; j<17; j++){
						if(bin[j]==1){
							bin.erase(bin.begin()+8);
							bin.erase(bin.begin()+8);
							bin.erase(bin.begin());
							bin.erase(bin.begin());
							bin.erase(bin.begin());

							while(bin.size()<16)bin.insert(bin.begin(),0);

							i=9;
							j=17;
						}
					}
				}
			}

			for(unsigned int j=0; j<bin.size(); j++){
				for(int i=0; i<4; i++){
					st += to_string(bin[j+i]);
				}
				stevila.push_back(st);
				st="";
				j += 3;
			}
		}
	}
	else if(bin[0]==1 && bin[1]==1 && bin[2]==1 && bin[3]==0){
		if(bin.size()<24 || bin[8]!= 1 || bin[9]!=0 || bin[16]!=1 || bin[17]!=0){
			cout<<"Napaka!"<<endl;
		}else{
			for(int i=3; i<9; i++){
				if(bin[i]==1){
					for(int j=10; j<17; j++){
						if(bin[j]==1){
							for(int x=18; x<25; x++){
								if(bin[x]==1){
									bin.erase(bin.begin()+16);
									bin.erase(bin.begin()+16);
									bin.erase(bin.begin()+8);
									bin.erase(bin.begin()+8);
									bin.erase(bin.begin());
									bin.erase(bin.begin());
									bin.erase(bin.begin());
									bin.erase(bin.begin());

									while(bin.size()<24)bin.insert(bin.begin(),0);

									i=9;
									j=17;
									x=25;
								}
							}
						}
					}
				}
			}

			for(unsigned int j=0; j<bin.size(); j++){
				for(int i=0; i<4; i++){
					st += to_string(bin[j+i]);
				}
				stevila.push_back(st);
				st="";
				j += 3;
			}
		}
	}
	else if(bin[0]==1 && bin[1]==1 && bin[2]==1 && bin[3]==0){
			if(bin.size()<24 || bin[8]!= 1 || bin[9]!=0 || bin[16]!=1 || bin[17]!=0 || bin[24]!=1 || bin[25]!=0){
				cout<<"Napaka!"<<endl;
			}else{
				for(int i=3; i<9; i++){
					if(bin[i]==1){
						for(int j=10; j<17; j++){
							if(bin[j]==1){
								for(int x=18; x<25; x++){
									if(bin[x]==1){
										for(int y=26; y<33; y++){
											if(bin[x]==1){
												bin.erase(bin.begin()+24);
												bin.erase(bin.begin()+24);
												bin.erase(bin.begin()+16);
												bin.erase(bin.begin()+16);
												bin.erase(bin.begin()+8);
												bin.erase(bin.begin()+8);
												bin.erase(bin.begin());
												bin.erase(bin.begin());
												bin.erase(bin.begin());
												bin.erase(bin.begin());
												bin.erase(bin.begin());

												while(bin.size()<24)bin.insert(bin.begin(),0);

												i=9;
												j=17;
												x=25;
												y=33;
											}
										}
									}
								}
							}
						}
					}
				}

				for(unsigned int j=0; j<bin.size(); j++){
					for(int i=0; i<4; i++){
						st += to_string(bin[j+i]);
					}
					stevila.push_back(st);
					st="";
					j += 3;
				}
			}
		}


	cout<<"Rezultat dekodiranja UTF-8 je: ";
	for(unsigned int i=0; i<stevila.size(); i++){

		if(stevila[i]=="0000"){
			cout<<0;
		}
		else if(stevila[i]=="0001"){
			cout<<1;
		}
		else if(stevila[i]=="0010"){
			cout<<2;
		}
		else if(stevila[i]=="0011"){
			cout<<3;
		}
		else if(stevila[i]=="0100"){
			cout<<4;
		}
		else if(stevila[i]=="0101"){
			cout<<5;
		}
		else if(stevila[i]=="0110"){
			cout<<6;
		}
		else if(stevila[i]=="0111"){
			cout<<7;
		}
		else if(stevila[i]=="1000"){
			cout<<8;
		}
		else if(stevila[i]=="1001"){
			cout<<9;
		}
		else if(stevila[i]=="1010"){
			cout<<"A";
		}
		else if(stevila[i]=="1011"){
			cout<<"B";
		}
		else if(stevila[i]=="1100"){
			cout<<"C";
		}
		else if(stevila[i]=="1101"){
			cout<<"D";
		}
		else if(stevila[i]=="1110"){
			cout<<"E";
		}
		else if(stevila[i]=="1111"){
			cout<<"F";
		}
	}
}



int main() {
	vector<char> hexvhod;
	vector<char> hexdekod;
	vector<int> bin;
	char input;
	const char* prva;
	const char* druga;
	string line;
	string linedekod;

	cout<<"Napišite heksadecimalno število za kodiranje: ";
	getline(cin, line);
	istringstream stream(line);
	while (stream >> input) hexvhod.push_back(input);


	string test = "";
	for(unsigned int i=0; i< hexvhod.size(); i++){
		string sym(1, hexvhod[i]);
		string sym1(1, hexvhod[i+1]);
		prva = sym.c_str();
		druga = sym1.c_str();
		if((strcmp(prva, "F")==0 && strcmp(druga, "F")==0) || (strcmp(prva, "F")==0 && strcmp(druga, "F")==0)){
			cout<<"Napaka! ff in fe nista dovoljeni!"<<endl;
			exit(1);
		}
		i++;
	}

	hexvbin(hexvhod, bin);

	for(unsigned int i=0; i<bin.size(); i++){
		cout<<bin[i];
	}
	cout<<endl;

	kodiranje(bin);
	cout<<endl;

	bin.clear();

	cout<<"Napišite heksadecimalno število za dekodiranje: ";
	getline(cin, linedekod);
	istringstream streamdekod(linedekod);
	while (streamdekod >> input) hexdekod.push_back(input);



	hexvbin(hexdekod, bin);

	for(unsigned int i=0; i<bin.size(); i++){
		cout<<bin[i];
	}
	cout<<endl;

	dekodiranje(bin);




	return 0;
}
